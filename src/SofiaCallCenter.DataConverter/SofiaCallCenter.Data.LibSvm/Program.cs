﻿using BulStem;
using CapitalCrawler.Export.Helpers;
using LibSvmHelper;
using LibSvmHelper.Helpers;
using SofiaCallCenter.Crawler.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;

namespace SofiaCallCenter.Data.LibSvm
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length > 0)
            {
                ArgumentsHelpers.Arguments arguments = new ArgumentsHelpers.Arguments(args);

                //List<string> labels = args[1].Split('|');
                string outputFile = @"D:\Programming\DataMining\mood-detection-bg\tools\capital-Signals-mood.liblinear";
                if (arguments.ContainsParameter("outputFile"))
                {
                    outputFile = arguments["outputFile"];
                }

                string inputCsvFile = string.Empty;
                if (arguments.ContainsParameter("inputCsvFile"))
                {
                    inputCsvFile = arguments["inputCsvFile"];
                }

                bool includeNeutralLabel = false;
                if (arguments.ContainsParameter("includeNeutralLabel"))
                {
                    includeNeutralLabel = bool.Parse(arguments["includeNeutralLabel"]); ;
                }

                bool allCaps = false;
                if (arguments.ContainsParameter("allCaps"))
                {
                    allCaps = bool.Parse(arguments["allCaps"]); ;
                }

                bool usePlainTokens = true;
                if (arguments.ContainsParameter("usePlainTokens"))
                {
                    usePlainTokens = bool.Parse(arguments["usePlainTokens"]);
                }

                bool useEmotIcons = false;
                if (arguments.ContainsParameter("useEmotIcons"))
                {
                    useEmotIcons = bool.Parse(arguments["useEmotIcons"]);
                }

                bool useStopWords = false;
                if (arguments.ContainsParameter("useStopWords"))
                {
                    useStopWords = bool.Parse(arguments["useStopWords"]); ; ;
                }

                bool useStemmer = false;
                if (arguments.ContainsParameter("useStemmer"))
                {
                    useStemmer = bool.Parse(arguments["useStemmer"]); ; ;
                }

                bool countPunctuation = false;
                if (arguments.ContainsParameter("countPunctuation"))
                {
                    countPunctuation = bool.Parse(arguments["countPunctuation"]); ; ;
                }

                bool useNGrams_2 = false;
                if (arguments.ContainsParameter("useNGrams_2"))
                {
                    useNGrams_2 = bool.Parse(arguments["useNGrams_2"]); ; ;
                }

                bool useNGrams_3 = false;
                if (arguments.ContainsParameter("useNGrams_3"))
                {
                    useNGrams_3 = bool.Parse(arguments["useNGrams_3"]); ; ;
                }
                                
                ExportCommentsFeaturesToLibSvm(SignalCategory.CategoryFull, SignalContent.Title,inputCsvFile,outputFile,  includeNeutralLabel, usePlainTokens, useStopWords, useStemmer, countPunctuation, useNGrams_2, useNGrams_3, allCaps, useEmotIcons);
            }
        }

        public static string GetCurrentAssimblyDir()
        {
                string codeBase = Assembly.GetExecutingAssembly().CodeBase;
                UriBuilder uri = new UriBuilder(codeBase);
                string path = Uri.UnescapeDataString(uri.Path);
                return Path.GetDirectoryName(path);
        }

        private static void ExportCommentsFeaturesToLibSvm(SignalCategory signalCategoryType, SignalContent signalContentType,string commentsCsvFile,string outputLibSvmFile,  bool includeNeutralLabel, bool usePlainToken, bool useStopWords, bool useStemmer, bool countPunctuation, bool useNGrams_2, bool useNGrams_3, bool allCaps, bool useEmotIcons)
        {
            string stopWordsFile =GetCurrentAssimblyDir()+ @"\Lexicons\stopwords_bg.txt";

            Console.WriteLine("Features:");


            if (usePlainToken)
                Console.WriteLine("- Bag of words");

            if (useEmotIcons)
                Console.WriteLine("- Emoticons");

            if (useStopWords)
                Console.WriteLine("- Exclude stopwords");


            if (useStemmer)
                Console.WriteLine("- Stemmed words");


            if (countPunctuation)
                Console.WriteLine("- Count punctuation signs");


            if (useNGrams_2)
                Console.WriteLine("- Ngrams - 2");


            if (useNGrams_3)
                Console.WriteLine("- Ngrams - 3");

            if (allCaps)
                Console.WriteLine("- AllCaps words count");

            //init database
            var signals = SignalsCsvHelper.ReadSignalsFromCsvFile(commentsCsvFile);


            var commentsClearText = signals
                                    .Select(c =>
                                        new
                                        {
                                            Id = c.Id,
                                            Category = GetSignalCategory(c,signalCategoryType),
                                            Text = GetSignalText(c, signalContentType)
                                        })
                                        .ToList();


            //filter Signals to test
            var signalsToProcess = commentsClearText
                                    .ToList();


            Console.WriteLine("");
            Console.WriteLine(string.Format("Signals count: {0}", signalsToProcess.Count));


            //load labels
            Dictionary<string, int> labels = GetLabelsWithInt(signalsToProcess.Select(c => c.Category).Distinct().ToList());
            Console.WriteLine(string.Format("Labels: {0}", labels.Count));


            var labelsWithNumber = labels.Select(l => new { Mood = l.Key, Count = signalsToProcess.Count(cc => cc.Category == l.Key) }).Distinct().ToList();
            foreach (var labelWithNumber in labelsWithNumber)
            {
                Console.WriteLine(string.Format(" {0} - {1}", labelWithNumber.Mood, labelWithNumber.Count));
            }
            //load stopwords
            List<string> stopWords = useStopWords ? LexiconReaderHelper.LoadWordsFromFile(stopWordsFile) : new List<string>();

            //init stemmer

            Stemmer stemmer = useStemmer ? new Stemmer(StemmingLevel.Medium) : null;

            List<SparseItemString> sparseItems = new List<SparseItemString>();
            foreach (var signal in signalsToProcess)
            {
                string commentText = signal.Text;
                if (!useEmotIcons)
                {
                    commentText = Regex.Replace(commentText, @"\[emo-[^\]]*\]", "");
                }

                SparseItemString item = new SparseItemString();
                item.Label = labels[signal.Category];

                //split on tokens
                List<string> commentTokens = commentText.Split(new char[] { ',', ' ', ';', ':', '\t', '\r', '\n', '(', ')', '?', '.', '!' }, StringSplitOptions.RemoveEmptyEntries).ToList();

                if (countPunctuation)
                {
                    SetFeatureToRegExMatchesCount("!{1}", "punct_singl_exclam", commentText, item);
                    SetFeatureToRegExMatchesCount("!{2,}", "punct_multi_exclam", commentText, item);

                    SetFeatureToRegExMatchesCount("\\?{1}", "punct_singl_question", commentText, item);
                    SetFeatureToRegExMatchesCount("\\?{2,}", "punct_multi_question", commentText, item);

                    SetFeatureToRegExMatchesCount("\\.", "punct_singl_dot", commentText, item);
                    SetFeatureToRegExMatchesCount("\\.{2,}", "punct_multi_dot", commentText, item);

                    SetFeatureToRegExMatchesCount("\\w", "stat_words_count", commentText, item);

                    SetFeatureToRegExMatchesCount(@"\[emo-[^\]]*\]", "emoticons_all", commentText, item);
                }

                //single tokens
                foreach (var token in commentTokens)
                {
                    string tokenKey = token.ToLower();

                    //filter stopwords
                    if (useStopWords && stopWords.Contains(tokenKey))
                    {
                        continue;
                    }

                    //whole word
                    if (usePlainToken)
                    {
                        item.IncreaseFeatureFrequency(tokenKey, 1);
                    }

                    //stemmer
                    if (useStemmer)
                    {
                        tokenKey = stemmer.Stem(tokenKey);
                        item.IncreaseFeatureFrequency(tokenKey, 1);
                    }
                }

                //ngrams - 2
                if (useNGrams_2 && (commentTokens.Count >= 2))
                {
                    string prefix = "2gram";
                    for (int i = 0; i < commentTokens.Count - 2; i++)
                    {
                        string ngramToken = string.Format("{0}_{1}_{2}", prefix, commentTokens[i], commentTokens[i + 1]);
                        item.IncreaseFeatureFrequency(ngramToken, 1m);
                    }
                }

                //ngrams - 3
                if (useNGrams_3 && (commentTokens.Count >= 2))
                {
                    string prefix = "3gram";
                    for (int i = 0; i < commentTokens.Count - 3; i++)
                    {
                        string ngramToken = string.Format("{0}_{1}_{2}_{3}", prefix, commentTokens[i], commentTokens[i + 1], commentTokens[i + 2]);
                        item.IncreaseFeatureFrequency(ngramToken, 1m);
                    }
                }

                sparseItems.Add(item);
            }

            ExportHelper.ExportToLibSvmFileFormat(sparseItems, true, outputLibSvmFile);
        }

        private static string GetSignalText(Crawler.Signal c, SignalContent signalContentType)
        {
            switch (signalContentType)
            {
                case SignalContent.Title:
                    return c.Title;
                    break;
                case SignalContent.Description:
                    return c.Description;
                    break;
                case SignalContent.TitleAndDescription:
                    return c.Title+" "+c.Description;
                    break;
                default:
                    return c.Title + " " + c.Description;
                    break;
            }
        }

        private static string GetSignalCategory(Crawler.Signal c, SignalCategory signalCategoryType)
        {
            switch (signalCategoryType)
            {
                case SignalCategory.CategoryFull:
                    return c.CategoryFull;
                    break;
                case SignalCategory.CategoryLevel1:
                    return c.CategoryLevel1;
                    break;
                case SignalCategory.CategoryLevel2:
                    return c.CategoryLevel2;
                    break;
                default:
                    return c.CategoryFull;
                    break;
            }
        }

        public static void SetFeatureToRegExMatchesCount(string pattern, string featureKey, string commentText, SparseItemString item)
        {
            var matchesSingleExclamation = Regex.IsMatch(commentText, pattern) ? Regex.Matches(commentText, pattern) : null;
            if (matchesSingleExclamation == null)
            {
                item.SetFeatureValue(featureKey, 0m);
                return;
            }

            int numberOfSingleExclamation = matchesSingleExclamation.Count;
            item.SetFeatureValue(featureKey, numberOfSingleExclamation * 1.0m);
        }

        public static Dictionary<string, int> GetLabelsWithInt(List<string> labels)
        {
            Dictionary<string, int> labelDictionary = new Dictionary<string, int>();
            int index = 0;
            foreach (var label in labels)
            {
                if (!labelDictionary.ContainsKey(label))
                {
                    index++;
                    labelDictionary.Add(label, index);
                }
            }

            return labelDictionary;
        }

        public static string ClearCommentBodyHtml(string inputData)
        {
            string patternEncodeEmo = "<img.*src=\\\"/i/emoticons/([a-z-]+).gif\\\"[^>]*>";

            string data = Regex.Replace(inputData, patternEncodeEmo, "[emo-$1]");
            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(data);
            string dataText = doc.DocumentNode.InnerText;
            dataText = Regex.Replace(dataText, @"(\\r|\\n|\\t|\r|\n|\t)", "");

            return dataText;
        }
    }
}
