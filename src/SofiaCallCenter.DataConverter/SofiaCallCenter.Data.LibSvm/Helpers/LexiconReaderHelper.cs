﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace CapitalCrawler.Export.Helpers
{
    public class LexiconReaderHelper
    {
        public static List<string> LoadWordsFromFile(string fileName)
        {
            List<string> words = new List<string>();
            string line;
            using (TextReader textReader = new StreamReader(fileName))
            {
                while ((line = textReader.ReadLine()) != null)
                {
                    words.Add(line.Trim());
                }
            }

            return words;
        }
    }
}
