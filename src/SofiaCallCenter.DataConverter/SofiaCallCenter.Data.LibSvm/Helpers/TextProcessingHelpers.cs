﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CapitalCrawler.Export
{
    public static class TextProcessingHelpers
    {
        /// <summary>
        /// Splits the given string into a list of substrings, while outputting the splitting
        /// delimiters (each in its own string) as well. It's just like String.Split() except
        /// the delimiters are preserved. No empty strings are output.</summary>
        /// <param name="s">String to parse. Can be null or empty.</param>
        /// <param name="delimiters">The delimiting characters. Can be an empty array.</param>
        /// <returns></returns>
        public static IList<string> SplitAndKeepDelimiters(this string s, char[] delimiters, bool keepEmpty)
        {
            var parts = new List<string>();
            if (!string.IsNullOrEmpty(s))
            {
                int iFirst = 0;
                do
                {
                    int iLast = s.IndexOfAny(delimiters, iFirst);
                    if (iLast >= 0)
                    {
                        if (iLast > iFirst)
                            ListAddWithCheck(parts, s.Substring(iFirst, iLast - iFirst), keepEmpty); //part before the delimiter
                        ListAddWithCheck(parts, new string(s[iLast], 1), keepEmpty);//the delimiter
                        iFirst = iLast + 1;
                        continue;
                    }

                    //No delimiters were found, but at least one character remains. Add the rest and stop.
                    ListAddWithCheck(parts, s.Substring(iFirst, s.Length - iFirst), keepEmpty);
                    break;

                } while (iFirst < s.Length);
            }

            return parts;
        }

        public static void ListAddWithCheck(List<string> list, string token, bool keepEmpty)
        {
            if (!keepEmpty)
            {
                if (string.IsNullOrWhiteSpace(token))
                {
                    return;
                }
            }

            list.Add(token);
        }
    }
}
