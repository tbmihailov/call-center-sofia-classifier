﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SofiaCallCenter.Data.LibSvm
{
    public enum SignalCategory
    {
        CategoryFull = 1,
        CategoryLevel1 = 2,
        CategoryLevel2 = 3
    }
}
