﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SofiaCallCenter.Data.LibSvm
{
    public enum SignalContent
    {
        Title = 1,
        Description = 2,
        TitleAndDescription = 3,
        TitleAndDescriptionSeparate = 4
    }
}
