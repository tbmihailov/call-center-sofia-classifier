﻿using SofiaCallCenter.Crawler.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SofiaCallCenter.Samples
{
    class Program
    {
        static void Main(string[] args)
        {
            string csvFile = "sofia-signals.csv";
            if (args.Length > 0)
            {
                csvFile = args[0];
            }

            var signals = SignalsCsvHelper.ReadSignalsFromCsvFile(csvFile);

            PrintStatisticsByCategoryLevel1(signals);
            PrintStatisticsByCategoryFull(signals);
        }

        private static void PrintStatisticsByCategoryLevel1(List<Crawler.Signal> signals)
        {
            var signalsGroupedByCategoryLevel1 = signals.GroupBy(s => s.CategoryLevel1);
            var signalsGroupedByCategoryLevel1Counts = signalsGroupedByCategoryLevel1.Select(s => new { Name = s.Key, Count = s.Count() });

            Console.WriteLine("---------------------------------");
            Console.WriteLine(string.Format("Signals grouped by CategoryLevel1:\n - Categories - {0} \n - Signals - {1}", signalsGroupedByCategoryLevel1Counts.Count(), signalsGroupedByCategoryLevel1Counts.Sum(s => s.Count)));
            foreach (var groupedItem in signalsGroupedByCategoryLevel1Counts)
            {
                Console.WriteLine(string.Format("{1} - {0}", groupedItem.Name, groupedItem.Count));
            }

            int maxCnt = signalsGroupedByCategoryLevel1Counts.Max(s => s.Count);
            int numberOfSignals = signalsGroupedByCategoryLevel1Counts.Sum(s => s.Count);
            double classifierCategoryLevel1BaseLine = (double)maxCnt / (double)numberOfSignals;
            Console.WriteLine("");
            Console.WriteLine(string.Format("Baseline - {0}", classifierCategoryLevel1BaseLine));
        }

        private static void PrintStatisticsByCategoryFull(List<Crawler.Signal> signals)
        {
            var signalsGroupedByCategory = signals.GroupBy(s => s.CategoryFull);
            var signalsGroupedByCategoryCounts = signalsGroupedByCategory.Select(s => new { Name = s.Key, Count = s.Count() });

            Console.WriteLine("---------------------------------");
            Console.WriteLine(string.Format("Signals grouped by CategoryLevel1:\n - Categories - {0} \n - Signals - {1}", signalsGroupedByCategoryCounts.Count(), signalsGroupedByCategoryCounts.Sum(s => s.Count)));
            foreach (var groupedItem in signalsGroupedByCategoryCounts)
            {
                Console.WriteLine(string.Format("{1} - {0}", groupedItem.Name, groupedItem.Count));
            }

            int maxCnt = signalsGroupedByCategoryCounts.Max(s => s.Count);
            int numberOfSignals = signalsGroupedByCategoryCounts.Sum(s => s.Count);
            double classifierCategoryBaseLine = (double)maxCnt / (double)numberOfSignals;
            Console.WriteLine("");
            Console.WriteLine(string.Format("Baseline - {0}", classifierCategoryBaseLine));
        }
    }
}
